const board = document.querySelector('#board')
const colors = ['rgb(245, 9, 206)', 'rgb(44, 9, 245)', 'rgb(10, 212, 215)', 'rgb(94, 229, 10)', 'rgb(229, 32, 10)', 'rgb(243, 10, 227)', 'rgb(165, 10, 243)', 'rgb(222, 247, 34)', 'rgb(13, 221, 117)']
const SQUARES_NUMBER = 682

for (let i = 0; i < SQUARES_NUMBER; i++) {
    const square = document.createElement('div')
    square.classList.add('square')

    square.addEventListener('mouseover', setColor)
    square.addEventListener('mouseleave', removeColor)

    board.append(square)
}

function setColor(event) {
    const element = event.target
    const color = getRandomColor()
    element.style.backgroundColor = color
    element.style.boxShadow = `0 0 2px ${color}, 0 0 20px ${color}`
}

function removeColor(event) {
    const element = event.target
    element.style.backgroundColor = `#1d1d1d`
    element.style.boxShadow = `0 0 2px #000`
}

function getRandomColor() {
    return colors[Math.floor(Math.random() * colors.length)]
}



